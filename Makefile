.PHONY: new feed publish

DATE_FILENAME=`date '+%Y%m%d'`
DATE_TEXT=`date '+%Y-%m-%d'`
TEMPLATE := template.gmi
EDITOR ?= /usr/bin/vim

push:
	sftp -b batchfile_sftp.txt gmisi3tch

new: 
	@echo "Dateiname? "; read TITLE_TEXT; \
	@cat $(TEMPLATE) | \
	sed "s/VAR_TITLE/$(TITLE_TEXT)/g" | \
	sed "s/VAR_PUB_DATE/$(DATE_TEXT)/g"  >> $(DATE_FILENAME)-$(TITLE_TEXT).gmi
	@$(EDITOR) $(DATE_FILENAME)-$(TITLE_TEXT).gmi

publish: feed
	sftp -b batchfile_sftp.txt gmisi3tch 

feed: 
	( \
	. ~/repos/gemfeed/venv/bin/activate; \
	gemfeed -b "gmi.si3t.ch/wurzelbit" -d posts -t "Wurzelbit's Gemlog" -o ../atom.xml; \
	)
