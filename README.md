# About this repository

This repository contains the posts I publish on a gemini server.
If you'd like to see my capsule visit gemini://gmi.si3t.ch/wurzelbit 

## My local setup

Locally I have the following directory structure and I'd like to explain how the file structure works

```
├── atom.xml
├── batchfile_sftp.txt
├── drafts
│   └── draft.gmi
├── index.gmi
├── Makefile
├── posts
│   └── published_post.gmi
├── README.md
└── template.gmi
```

- The `atom.xml` contains a generated feed. This feed is generated with https://tildegit.org/solderpunk/gemfeed
- `batchfile_sftp.txt` contains all the files which should be pushed onto the sftp server
- The `drafts` folder contains different posts I started to write but did not finish. This files are neither pushed to the gemini server nor to the git repository
- `index.gmi` is just the startpage for gemini
- The `Makefile` can be used to generate a feed and publish all the posts
- Inside the folder `posts` I have all my posts which should be published.
- `README.md` is this file :-)
- `template.gmi` contains a small template for my gemini posts. The file creation should in the future be automatic with the Makefile.
